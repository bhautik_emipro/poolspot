from datetime import datetime
from operator import itemgetter

import openerp
from openerp import SUPERUSER_ID
from openerp import tools
from openerp.addons.base.res.res_partner import format_address
from openerp.osv import fields, osv, orm
from openerp.tools.translate import _
from openerp.tools import email_re, email_split


class crm_lead(osv.osv):
    _inherit ='crm.lead'
    _columns = {
                'pool_count' : fields.integer('Pool Count'),
                }