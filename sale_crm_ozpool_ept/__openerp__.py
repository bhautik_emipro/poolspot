{
    "name": "Sale Crm Ext. EPT",
    "description": """
Extention of Sales crm module 
""",
    "author": "Emipro Technologies Pvt. Ltd",
    "version": "0.1",
    "depends": ["sale_crm"],
    "init_xml": [],
    "update_xml": [
                   'view/crm_lead.xml',
    ],
    "demo_xml": [],
    "installable": True,
}
