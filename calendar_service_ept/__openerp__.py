{
    'name': 'Calendar_Service',
    'version': '1.0',
    'category': 'Calendar',
    'description': """
This is extention of calendar service module
""",
    'author': 'Emipro Technologies Pvt. Ltd.',
    'website': 'www.emiprotechnologies.com',
    'depends': [
        'calendar_service'
    ],
    'data': [
        'security/ir.model.access.csv',
        'view/calendar_service.xml',
        'view/calendar_service_recurrent.xml'
        
    ],
    'installable': True,
    'application': True,
    'auto_install': False,
}
