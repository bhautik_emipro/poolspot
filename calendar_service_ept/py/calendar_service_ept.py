from openerp import fields,models,api,_
from datetime import datetime, timedelta
from exceptions import Warning
from pygments.lexer import _inherit
from pkg_resources import require


#from openerp.addons.calendar_service.models import calendar_service 

 
#@api.model
#def create_ept(self,vals):
#    res = super(models.Model, self).create(vals)
#     res = self.env['calendar.service.recurrent'].create(vals)
#    return res
 
 
#calendar_service.calendar_service_recurrent.create = create_ept    



class calendar_service_ept(models.Model):
    _inherit = 'calendar.service'
    
    
    
   # product_ids = fields.Many2many('product.product',string="Products")
   # p1_ids = fields.Many2many('product.product',string="Service Product")
    service_product_ids = fields.One2many('service.product.qty','calendar_service_ept_id',string="Product")
    
    start_time = fields.Datetime('Requested Date', required=True)
    end_time = fields.Datetime('Ending at', required=False)
    description = fields.Text('Description')
    
    
    @api.one
    @api.constrains('start_time', 'end_time')
    def _check_time(self):
        return True
    
    @api.one
    def close_state(self):
        if self.work_type == 'wait':
            raise Warning(_("Service with Waiting type can't be closed!"))
                   
        
           
        cal_serv_cal = self.env['calendar.service.calendar']   
            
        # avoid forcing default_state values when creating sale orders
        order_obj = self.env['sale.order'].with_context({
        key: val
        for key, val in self.env.context.iteritems()
        if not (isinstance(key, basestring) and key == 'default_state')
        })
        line_obj = self.env['sale.order.line'].with_context({
        key: val
        for key, val in self.env.context.iteritems()
        if not (isinstance(key, basestring) and key == 'default_state')                
        })
    
        vals = {'partner_id': self.partner_id.id, 'date_order': self.start_time,
        'pricelist_id': self.partner_id.property_product_pricelist.id,
        'user_id': self.user_id.id, 'calendar_service_id': self.id,
        }
        order = order_obj.create(vals)
        qty = 0.0
        for work in self.work_ids:  
            start_time = cal_serv_cal.str_to_dt(work.start_time)
        #   end_time = cal_serv_cal.str_to_dt(work.end_time)
        #   qty += round(cal_serv_cal.get_duration(start_time, end_time), 3) #converting duration as qty in hours
            line_vals = {'product_id': work.product_id.id,'state': 'draft', 'order_id':order.id}
            result= line_obj.product_id_change_with_wh([],False,work.product_id.id,qty)#On Change
            line_obj.create(line_vals) 
        
        if self.service_product_ids:    
        
            for product_id in self.service_product_ids:              
                line_vals = {'product_id': product_id.product_id.id, 'product_uom_qty':product_id.qty, 'state': 'draft', 'order_id':order.id}
                result= line_obj.product_id_change_with_wh([],False,product_id.id,product_id.qty)
                line_obj.create(line_vals)
        
                       
            if self.order_id.state == 'draft':
                order = self.order_id.write(vals)
                
        self.state = 'done'
  #     for work in self.work_ids:
  #          work.state = 'done'
    
    @api.one
    @api.constrains('work_ids')
    def _check_works(self):
        return True


               
class calendar_service_recurrent_rule(models.Model):
        _inherit='calendar.service.recurrent.rule'  
 
        #Change Require False and Many2one
        partner_id = fields.Many2one('res.partner', 'Customer', domain=[('customer', '=', True)] , required=False)
        
        #Edit above the field
        partner_ids = fields.Many2many('res.partner',string="Customer",required=True)


        
