from openerp import fields,models,api,_
from datetime import datetime, timedelta
from exceptions import Warning
from pygments.lexer import _inherit


class calendar_service_work(models.Model):
    _inherit='calendar.service.work'
    
    product_id = fields.Many2one('product.product',"Service Product")
#   product_ids = fields.Many2many('product.product',"Service Product")
    
    # Override Field
    employee_id = fields.Many2one('hr.employee', 'Responsible', required=False)
    start_time = fields.Datetime('Starting at', required=False)
    end_time = fields.Datetime('Ending at', required=False)
    calendar_attention = fields.Char('Calendar Warning', compute=False)
    service_order_id = fields.Many2one('calendar.service',string="Service")
    
    @api.one
    @api.constrains('start_time','service_id')
    def _check_time(self):
        """
        Lets to set works time only in service time constraints
        """
        cal_serv_cal = self.env['calendar.service.calendar']
        start_time = cal_serv_cal.str_to_dt(self.start_time)
        serv_start_time = cal_serv_cal.str_to_dt(self.service_id.start_time)
        if start_time < serv_start_time: 
            raise Warning(_("Work start time can't go out of service time constraints!"))
    
    
    @api.onchange('service_id')
    def onchange_service_id(self):
        if self.service_id:
            self.start_time = self.service_id.start_time
            self.partner_id = self.service_id.partner_id and self.service_id.partner_id.id or False
            self.work_type = self.service_id.work_type
            if self.partner_id:
                for address in self.partner_id.address_archive_ids:
                    if address.current:
                        self.address_archive_id = address.id
                self.note = self.partner_id.comment
                self.attention = self.partner_id.attention
    
            
    @api.one
    @api.constrains('start_time', 'employee_id', 'state', 'work_type')
    def _check_resource(self):
        """
        Does double resource checking to see if resource (employee)
        is already taken for specific time. First check is to see between
        already created records. Second check is to see if recurrent rule
        is set for that time, the record is being created for.
        """
        cal_serv_cal = self.env['calendar.service.calendar']
        if self.work_type != 'wait':
            recs = self.search([('id', '!=', self.id), ('employee_id', '=', self.employee_id.id), 
                ('state', 'in', ('open', 'done')), ('work_type', '!=', 'wait'),('start_time','=',self.start_time)])
            for rec in recs:
                start_time = cal_serv_cal.set_tz(datetime.strptime(rec.start_time, "%Y-%m-%d %H:%M:%S"))
#                 end_time = cal_serv_cal.set_tz(datetime.strptime(rec.end_time, "%Y-%m-%d %H:%M:%S"))
                warn_str = "%s Already Assigned to %s in Service %s for %s" % \
                    (rec.employee_id.name, start_time, rec.service_id.name, rec.service_id.partner_id.name)
                raise Warning(_(warn_str))
            # Check if there is cancelled record in a place new record is being assigned. Pass it if true
            cancel_pass = False
            cancelled_rec = self.search([
                ('id', '!=', self.id), ('employee_id', '=', self.employee_id.id), ('state', '=', 'cancel'), 
                ('start_time', '=', self.start_time)])
            if cancelled_rec:
                cancel_pass = True
            recurrent = self.env['calendar.service.recurrent'].search([('active', '=', True)])
            for r in recurrent:
#                 if recurrent: 
#                     if not recurrent.next_gen_time:
#                         raise Warning(_("You need to Initially generate Recurrent Calendar\n"
#                             "before creating any services or works!")) 
#                 else:
#                     msg = _("Recurrent Calendar is not created. Create one (Recurrent Services Config).")    
#                     raise Warning(msg)
    
                #Checks Rules if that time is already reserved for any of it
                if self.state == 'open' and self.work_type != 'wait' and self.start_time >= r.next_gen_time and not cancel_pass and not self.ign_rule_chk:
                    start_time = cal_serv_cal.set_tz(datetime.strptime(self.start_time, 
                        cal_serv_cal.get_dt_fmt()))
                    weekday = cal_serv_cal.get_rev_weekday(start_time.weekday()) #get weekday in calendar.service.calendar
                    start_h = start_time.hour
                    start_min = round(float(start_time.minute) / 60, 2)
                    time_from = float(start_h + start_min)
                    end_time = cal_serv_cal.set_tz(datetime.strptime(self.end_time, 
                        cal_serv_cal.get_dt_fmt())) or False
                    end_h = end_time.hour
                    end_min = round(float(end_time.minute) / 60, 2)
                    time_to = float(end_h + end_min)
                    next_gen_time = cal_serv_cal.set_tz(cal_serv_cal.str_to_dt(r.next_gen_time))
                    cal_recs = cal_serv_cal.search([('employee_ids', 'in', [self.employee_id.id]), 
                        ('rule_id.recurrent_id', '=', r.id), ('weekday', '=', weekday), 
                        ('time_from', '<', time_to), ('time_to', '>', time_from)])
                    for cal_rec in cal_recs:
                        if cal_rec.second_week and start_time >= next_gen_time:
                            # Find if 'every second' week matches week that resource is being assigned
                            next_gen_time_m = next_gen_time - timedelta(days=next_gen_time.weekday())
                            start_time_m = start_time - timedelta(days=start_time.weekday())
                            week_diff = (start_time_m - next_gen_time_m).days / 7 #get difference in weeks
                            #check if week is reserved or not
                            if (week_diff % 2 == 0 and cal_rec.last_week_gen) or (week_diff % 2 != 0 and not cal_rec.last_week_gen):
                                continue
                        raise Warning(_("There is rule named '%s' that have assignet resource \n"
                            "%s to work at %s from %s to %s !" % (cal_rec.rule_id.name, self.employee_id.name, 
                            cal_serv_cal.get_weekday(weekday), cal_serv_cal.float_to_time(cal_rec.time_from), cal_serv_cal.float_to_time(cal_rec.time_to))))

    

    
            
            
            
