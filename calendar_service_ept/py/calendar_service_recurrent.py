from openerp import fields,models,api,_
from datetime import datetime, timedelta
from exceptions import Warning
from pygments.lexer import _inherit
from pkg_resources import require
from dateutil.relativedelta import relativedelta



class calendar_service_recurrent(models.Model):
    _inherit='calendar.service.recurrent'
    
   # ign_second_week = fields.Boolean('Ignore Second Week Check') #used to second week constrain when generating calendar
    
    
    @api.model
    def create(self, vals):
        return super(models.Model, self).create(vals)
    
    @api.one
    def create_service(self,service_obj, service_work_obj, start_time, 
        cal_rec, rule, current_address, change_time=None):
        """
        Helper method for creating services and its works 
        for generation methods.
        """
        for partner_id in rule.partner_ids:
            service = service_obj.create({
                'start_time': start_time, 
            #   'end_time': end_time,
                'user_id': rule.user_id.id, 'work_type': 'recurrent' if cal_rec else 'one',
                'rule_calendar_id': cal_rec.id if cal_rec else None, 
                'product_id': change_time.product_id.id if change_time else cal_rec.product_id.id, #exception for early change_time use
                'partner_id': partner_id.id,
            })
            if change_time:
                cal_rec = change_time
            for empl in cal_rec.employee_ids:
                if rule.partner_ids:
                    for partner_obj in rule.partner_ids: 
                        service_work_obj.create({
                            'start_time': start_time, 
                    #        'end_time': end_time,
                            'employee_id': empl.id, 'work_type': 'recurrent',
                            'address_archive_id': current_address.id,
                            'partner_id': rule.partner_id.id, 'note': rule.partner_id.comment, 
                            'attention': rule.partner_id.attention, 'service_id': service.id,
                            'ign_rule_chk': True, #ign_rule_chk lets prevent istelf constraining.                                
                        })
                else:
                    service_work_obj.create({
                        'start_time': start_time, 
                    #    'end_time': end_time,
                        'employee_id': empl.id, 'work_type': 'recurrent',
                        'address_archive_id': current_address.id,
                       # 'partner_id': partner_obj.id, 'note': partner_obj.comment, 
                       # 'attention': partner_obj.attention, 'service_id': service.id,
                        'ign_rule_chk': True, #ign_rule_chk lets prevent istelf constraining.                                
                    })         
    
    

           
    
    @api.one
    def generate_recurrent(self):
        """
        Generates recurrent services from specified 
        recurrent calendar rules. Can generate weekly,
        or every second week repeating calendar services
        with their works. 
        """
        if self.active:
            cal_serv_cal = self.env['calendar.service.calendar']
            now1 = cal_serv_cal.set_utc(datetime.today() + timedelta(hours=1), check_tz=False)
            service_obj = self.env['calendar.service']
            service_work_obj = self.env['calendar.service.work']

            self.ign_second_week = True #to let interchanging last_week_gen value
            # Set next generate time
            if not self.next_gen_time:
                self.set_next_gen_time()
            
                            
            for rule in self.rule_ids:
                #for partner in self.rule_ids.partner_ids:
                for partner in rule.partner_ids:
                    current_address = self.env['res.partner.address_archive'].search(
                        [('partner_id', '=', partner.id), ('current', '=', True)])
                for cal_rec in rule.calendar_ids:
                   # skip_week = True
                    
                  #  cal_rec.last_week_gen = cal_serv_cal._resolve_week_skip(self._get_week_range(),skip_week)
                    # Assign init skip. Its either True or False.
                    skip_week = cal_rec.last_week_gen
                    
                    cal_rec.last_week_gen = cal_serv_cal._resolve_week_skip(self._get_week_range(), skip_week)
                    for week in range(self._get_week_range()):
                        #skip_week = cal_rec.last_week_gen
                        
                       # skip_week = cal_rec.last_week_gen
                        
                        if not self.init:
                            ref_time = datetime.strptime(self.next_gen_time, "%Y-%m-%d %H:%M:%S") + timedelta(weeks=week, days=1) #add day to jump to next week
                            now1 = cal_serv_cal.set_utc(datetime.strptime(self.next_gen_time, "%Y-%m-%d %H:%M:%S"), check_tz=False)
                        else:
                            ref_time = datetime.today() + timedelta(weeks=week)                            
                        start_time = cal_rec.relative_date(ref_time, 
                            cal_rec.get_weekday(cal_rec.weekday, name=False), cal_rec.time_from)
                        end_time = cal_rec.relative_date(ref_time, 
                            cal_rec.get_weekday(cal_rec.weekday, name=False), cal_rec.time_to)
                        if start_time >= now1:
                            if cal_rec.second_week: #checking if need to generate every or second week
                                if not skip_week:
                                    self.create_service(service_obj, service_work_obj, start_time, 
                                        cal_rec, rule, current_address)   
                                skip_week = not skip_week
                            if cal_rec.month and week % 4 == 0:   
                                self.create_service(service_obj, service_work_obj, start_time,  
                                    cal_rec, rule, current_address)
#                             else:
#                                 self.create_service(service_obj, service_work_obj, start_time, end_time, 
#                                     cal_rec, rule, current_address)                                
            if self.init:
                self.init = False
            else:
                self.set_next_gen_time()
                self.ign_second_week = False #Stop ignoring second_week

        else:
            raise Warning(_("Inactive Recurrent Calendar can\'t be generated!"))
    
    
#        
 
    @api.one
    @api.constrains('active')
    def _check_active(self):
        if self.active:
            recurrent_recs = self.search([('id', '!=', self.id), ('active', '=', True)])
