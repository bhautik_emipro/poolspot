from openerp import fields,models,api,_
from datetime import datetime, timedelta
from exceptions import Warning

class product_qty(models.Model):
    _name = 'service.product.qty'
        
    product_id = fields.Many2one('product.product',string="Product")
    calendar_service_ept_id = fields.Many2one('calendar.service',"Calendar service")
    qty = fields.Float(string="Quantity")
