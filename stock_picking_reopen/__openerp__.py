{ 'sequence': 500,

"name" : "Picking Reopen",
"version" : "8.2",
"author" : "Emipro Technologies",
"category": 'Warehouse Management',
'complexity': "normal",
"description": """
Allows reopening of uninvoiced and canceled pickings.
=====================================================

This module allows to reopen (set to Ready to Process) uninvoiced pickings
as long as no other stock moves for products with cost method "average price" 
of this picking are confirmed.
The intention is to allow to correct errors or add missing info which becomes 
usually only visible after printing the picking.

""",
'website': 'http://www.emiprotechnologies.com',
"depends" : ["account_invoice_reopen"],
'init_xml': [],
'data': [
        ],
'demo_xml': [],
'installable': True,
'auto_install': False,
}

# vim:expandtab:smartindent:tabstop=4:softtabstop=4:shiftwidth=4:
