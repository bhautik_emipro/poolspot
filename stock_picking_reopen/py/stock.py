from openerp import models,api,_,fields
import time
from openerp.exceptions import Warning


class stock_location_route(models.Model):
    _inherit='stock.location.route'
    
    is_drop_shipping=fields.Boolean('Is Drop Shipping',default=False)

class stock_picking(models.Model):
    _inherit = 'stock.picking'

    @api.multi
    def allow_reopen(self):
        move_line_obj = self.env['stock.move']
        for picking in self:
            if picking.state=='done':
                if picking.picking_type_id.code=='incoming':
                    for move in picking.move_lines:
                        if move.procurement_id and move.procurement_id.sale_line_id and move.procurement_id.sale_line_id.route_id and move.procurement_id.sale_line_id.route_id.is_drop_shipping:
                            raise Warning("You are not allow to reset dropshipping line")
                        for quant in move.quant_ids:
                            #changes in condition quant.location_id.usage=='internal':
                            if quant.location_id.usage!='internal':
                                raise Warning("You are not allow to reset delivered move %s"%(move.product_id.name))
                elif picking.picking_type_id.code=='outgoing':
                    for move in picking.move_lines:
                        if move.procurement_id and move.procurement_id.sale_line_id and move.procurement_id.sale_line_id.route_id and move.procurement_id.sale_line_id.route_id.is_drop_shipping:
                            raise Warning("You are not allow to reset dropshipping line")
                        for quant in move.quant_ids:
                            if quant.qty > 0.0 and  quant.location_id.usage=='internal':
                                raise Warning("You are not allow to reset returned move %s"%(move.product_id.name))                    
        return True
    
    @api.multi
    def action_reopen(self):
        """ Changes picking and move state from done to confirmed.
        @return: True
        """
        self.allow_reopen()
        attachment_obj = self.env['ir.attachment']

        now = ' ' + _('Invalid') + time.strftime(' [%Y%m%d %H%M%S]')
        for picking in self:
            if picking.picking_type_id.code=='incoming' and picking.state=='done':
                for move in picking.move_lines:
                    move.quant_ids.unlink()
                    if move.account_move_ids:
                        move.account_move_ids.button_cancel()
                        move.account_move_ids.unlink()
            elif picking.picking_type_id.code=='outgoing' and picking.state=='done':
                for move in picking.move_lines:
                    for quant in move.quant_ids:
                        if quant.propagated_from_id:
                            quant.propagated_from_id.unlink()
                            quant.unlink()
                        else:                             
                            quant.write({'location_id':move.location_id.id})
                    move.write({'quant_ids':[(5,)]})
                    if move.account_move_ids:
                        move.account_move_ids.button_cancel()
                        move.account_move_ids.unlink()
            else:                                          
                picking.do_unreserve()                
            picking.move_lines and picking.move_lines.write({'state':'cancel'})
            attachments = attachment_obj.search([('res_model','=','stock.picking'),('res_id','=',picking.id)])
            for attachment in attachments:
                vals = {
                        'name': attachment.name.replace('.pdf', now+'.pdf'),
                        'datas_fname': attachment.datas_fname.replace('.pdf.pdf', now+'.pdf.pdf')
                }
                attachment.write(vals)            
        return True
    

class stock_move(models.Model):
    _inherit="stock.move"

    account_move_ids=fields.One2many('account.move','stock_move_id',string="Account Moves")
    
