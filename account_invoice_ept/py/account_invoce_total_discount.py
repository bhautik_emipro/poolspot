import itertools
from lxml import etree

from openerp import models, fields, api, _
from openerp.exceptions import except_orm, Warning, RedirectWarning
from openerp.tools import float_compare
import openerp.addons.decimal_precision as dp

class account_invoice_total_discount(models.Model):
    _inherit = 'account.invoice'
    
    @api.model
    def get_account_invoice_total_discount(self):
        total_discount=0
        for line_obj in self.invoice_line:
            total_discount=total_discount+(line_obj.quantity * line_obj.price_unit * line_obj.discount) / 100
        return total_discount 
     
    
    
    
