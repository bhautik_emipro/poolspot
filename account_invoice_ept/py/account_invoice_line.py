import itertools
from lxml import etree

from openerp import models, fields, api, _
from openerp.exceptions import except_orm, Warning, RedirectWarning
from openerp.tools import float_compare
import openerp.addons.decimal_precision as dp

class account_invoice_line(models.Model):
    _inherit = 'account.invoice.line'
    
    @api.model
    def get_discount_ept(self, line_obj):
        return (line_obj.quantity * line_obj.price_unit * line_obj.discount) / 100
     
    
    
    
