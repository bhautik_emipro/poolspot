{
    'name' : 'Account Invoice EPT',
    'version' : '1.1',
    'author' : 'Emipro Technologies Pvt. Ltd.',
    'category' : 'Report',
    'description' : """
    * Reconciliation process by partner


Processes like maintaining general ledgers are done through the defined Financial Journals (entry move line or grouping is maintained through a journal) 
for a particular financial year and for preparation of vouchers there is a module named account_voucher.
    """,
    'website': 'https://www.emiprotechnologies.com',
    'depends' : ['report','account'],
    'data': [
             "view/account_invoice_report_view.xml",
             "view/account_invoice_report.xml"
    ],
    'qweb' : [
      
    ],
    
    'installable': True,
    'auto_install': False,
}