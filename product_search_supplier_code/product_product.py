from openerp import models,api

class product_product(models.Model):
    
    _inherit='product.product'
    
    def name_search(self, cr, user, name='', args=None, operator='ilike', context=None, limit=100):
        if not context:
            context={}
        if args and args[0] and args[0][0]=='purchase_ok':
            context.update({'from_purchase_order':True})
        res=super(product_product,self).name_search(cr,user, name=name, args=args, operator=operator, context=context, limit=limit)
        
        return res
    
    def search(self, cr, uid, args, offset=0, limit=None, order=None, context=None, count=False):
        args1=args
        if not context:
            context={}
        if context.get('from_purchase_order',False):
            for arg in args:
                if arg[0] and arg[0]=='default_code':
                    args1=['|',('seller_ids.product_code', 'ilike',arg[2])]+args
                    
        res=super(product_product,self).search(cr, uid, args1, offset=offset, limit=limit, order=order, context=context, count=count)
         
        return res
