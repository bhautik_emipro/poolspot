from openerp import models, fields, api, _
from datetime import datetime, timedelta


class sale_order_job(models.TransientModel):
    _name = 'sale.order.job.ept'
    _description = 'Add Job into the Sale Order'
    
    sale_order_id = fields.Many2one('sale.order',string="Sale-Order")
    
    
    @api.multi
    def add_to_order(self):
        line_obj = self.env['sale.order.line']
        qty = 1
        service_order_job = self.env['service.order.jobs'].search([('id','=',self._context.get('job_id'))])
        if service_order_job.product_ids:
            for product_id in service_order_job.product_ids:
                line_vals = {'product_id':product_id.id,'job_id':service_order_job.id,'product_uom_qty':qty,'order_id':self.sale_order_id.id } 
                line_obj.create(line_vals)
        
        service_order_job.set_job = True
        service_order_job.sale_order =  self.sale_order_id.id
        
        # set the one argument job id (service_order_job).
        self.env['service.order.jobs'].create_analytical_account(service_order_job) 
        
        
    
    

    