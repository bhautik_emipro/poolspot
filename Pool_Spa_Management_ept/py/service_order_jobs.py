from openerp import api, fields, models, _
from datetime import datetime, timedelta
from pychart.arrow import default
from openerp.exceptions import Warning
from openerp.exceptions import except_orm

import base64
import werkzeug
from urlparse import urljoin
from urllib import urlencode
import new

#from openerp.tools.image import new

class service_order_jobs(models.Model):
    _name = 'service.order.jobs'
    _inherit = 'mail.thread'
    _description = 'Jobs Created'
    
    
    name = fields.Char(string='Order Reference', required=True, copy=False, readonly=True,index=True, default=lambda self: _('New'))
    start_date = fields.Date(string='Date',default=datetime.now())
    job_id = fields.Many2one('add.jobs',string='Job')
    product_id = fields.Many2one('product.product',"Service Product")
    product_ids = fields.Many2many('product.product',string="Service Product")
    start_time = fields.Float(string='Time')
    calendar_service_id = fields.Many2one('calendar.service',string='Service Order')
    state = fields.Selection([('draft','Draft'),('open','Open'),('pending','Pending'),('done','Done'),('cancel','Cancel')],default='draft',track_visibility='onchange')
    user_id = fields.Many2one('res.users',string="Salesman",default=lambda self: self.env.user and self.env.user.id or False)
    image = fields.Binary("image")
    color =  fields.Integer('Color Index')
  #  sale_order_line_id = fields.Many2one('sale.order.line',string="sale_order_id")
    set_job = fields.Boolean(string="job done",default=False)
    sale_order = fields.Many2one('sale.order',string="Sale-Order")
    

    @api.model
    def create(self, vals):
        if vals.get('name', 'New') == 'New':
            vals['name'] = self.env['ir.sequence'].next_by_code('service.order.jobs') or 'New'
            """ Use For the Assign the Sequence Number to Job """
            
        result = super(service_order_jobs, self).create(vals)
        return result
        
    
    
    @api.one
    def done_state(self):
        self.state = 'done'
        
    @api.one
    def pending_state(self):
        self.state = 'pending'
    
    @api.multi    
    def open_state(self):
        self.state = 'open'
        
        author_id = self.env['res.users'].browse(self._uid).partner_id.id
        user_obj = self.user_id
        query = {'db': self._cr.dbname}
        fragment = {
            'id': self.id,
            'login': user_obj.login or False,
            'model': 'service.order.jobs',
            }
            
        base_url_ept = self.env['ir.config_parameter'].get_param('web.base.url')
            
        url = urljoin(base_url_ept, "/web?%s#%s" % (werkzeug.url_encode(query), werkzeug.url_encode(fragment)))
        self.env['mail.message'].create({
                                       'author_id' : author_id,
                                       'date' : datetime.now(),
                                       'record_name':"Job Created : %s"%(self.name),
                                       'res_id':self.id,
                                       'model':'service.order.jobs',
                                       'notified_partner_ids' : [(6, 0, [user_obj.partner_id.id])],
                                       'body' : "<b>Service Order :</b> %s <br/> <b> Customer :</b> %s"%(self.calendar_service_id.name,self.calendar_service_id.partner_id.name)
                                       })
    
    
    #==============================================================================================
    #  first check, if sale order id found in related service ? if yes, than is that order 
    # in draft state ? if yes, than add those service products of job in that order
    #  invisible add to quote button 
    #  if no sale order found or that order is not in draft state, than find another sales order of 
    # that customer,which is in draft state show in wizard select one of the sale order , 
    # if found that add those related service products 
    # into that order. if no orders found, than create new order, add related service 
    # product in that order   
    #===============================================================================================
    @api.multi
    def add_to_quote(self):
        qty = 1
        line_obj = self.env['sale.order.line']
        service_order = self.env['calendar.service'].search([('job_ids','in',self.id)])
        if service_order :
            sale_order = self.env['sale.order'].search([('calendar_service_id','=',service_order.id)])
            
              
            if sale_order.state == 'draft':
                
                line_obj = self.env['sale.order.line']
                if self.product_ids:
                    for product_id in self.product_ids:
                        line_vals = {'product_id':product_id.id,'job_id':self.id ,'product_uom_qty':qty, 'state': 'draft', 'order_id':sale_order.id } 
                        line_obj.create(line_vals)
                        self.set_job = True
                    self.sale_order = sale_order.id
                
                self.create_analytical_account()
            
            sale_order_id = self.env['sale.order'].search([('partner_id','=',service_order.partner_id.id),('state','=','draft')])
            if sale_order_id :
                return {
                        'name':_("Add Job"),
                        'view_mode': 'form',                           
                        'view_type': 'form',
                        'view_id':self.env.ref('Pool_Spa_Management_ept.sale_order_job_wizard_view_ept').id,
                        'res_model': 'sale.order.job.ept',
                        'type': 'ir.actions.act_window',
                        'context':{'partner_id':self.calendar_service_id.partner_id.id,'job_id':self.id},
                        'target': 'new',
                        }
            
            else :
                if self.product_ids :
                    vals = {'partner_id': service_order.partner_id.id,'date_order': service_order.start_time,
                            'pricelist_id': service_order.partner_id.property_product_pricelist.id,
                            'user_id': service_order.user_id.id, 'calendar_service_id': service_order.id,
                            }
                    sale_order = sale_order.create(vals)
                    self.sale_order = sale_order.id # Set the Sale order  
                    for product_id in self.product_ids:
                            line_vals = {'product_id':product_id.id,'job_id':self.id ,'product_uom_qty':qty, 'state': 'draft', 'order_id':sale_order.id } 
                            line_obj.create(line_vals)
                            self.set_job = True    
    
                    self.create_analytical_account()
    
    # Put the Link in Kanban View Click on that go to the sale-order.                
    @api.multi
    def get_sale_order(self):
        return{
            'name':_("Sale Order"),
            'view_mode': 'form',                           
            'view_type': 'form',
            'res_model': 'sale.order',
            'type': 'ir.actions.act_window',
            'res_id':self.sale_order.id
            }
    
    # Can not Delete the Done Job Popup the warnig message when delete the done job..    
    @api.multi
    def unlink(self):
        for job in self:
            if job.state == 'done':
                raise except_orm('Warning..!','You can not delete the done Job')
            
            
    @api.model
    def create_analytical_account(self,job_id):
        record = self.env['account.analytic.line']
        user_id = self.env['hr.employee'].search([('user_id','=',self.user_id.id)],limit = 1)
        if user_id:  
            vals = {'name':job_id.name,'account_id':self.env.ref('__export__.account_analytic_account_14').id,'date':job_id.start_date,'journal_id':user_id.journal_id.id,'user_id':job_id.user_id.id,'amount':job_id.start_time,'product_id':user_id.product_id.id,'unit_amount':1}
        return record.create(vals)
        
        