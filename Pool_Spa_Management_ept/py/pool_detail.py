from openerp import api, fields, models, _

Cleaner_Type = [('in_floor','In-Floor'),('pressure','Pressure'),('robotic','Robotic'),('suction','Suction')]
Heater_Type = [('gas','Gas'),('heat_pump','Heat Pump'),('solar','Solar')]
class pool_detail(models.Model):
    _name = 'pool.detail'
    _description = 'Pool Detail/Profile'
    _rec_name = 'site_name'
    

    site_name = fields.Char(string='Site Name')
    pool_volume = fields.Float(string='Pool Volume')
    pool_type = fields.Selection([('in_ground',' In-Ground'),('above_ground',' Above-Ground')],'Pool Type',default='in_ground')
    pool_surface_id = fields.Many2one('pool.surface',string='Pool Surface')
    sanitising_method_id = fields.Many2one('enter.sanitising.method',string='Sanitising Method')
    sanitising_product_brand_id = fields.Many2one('product.brand',string='Product Brand')
    sanitising_product_id = fields.Many2one('product.product',string='Product')
    sanitising_installation_date = fields.Date(string='Sanitising Installation Date')
    sanitising_notes = fields.Text(string='Notes')
    pump_product_brand_id = fields.Many2one('product.brand',string='Product Brand')
    pump_product_id = fields.Many2one('product.product',string='Product')
    pump_installation_date = fields.Date(string='Pump Installation Date')
    pump_notes = fields.Text(string='Notes')
    filter_type = fields.Selection([('media_type',' Media Type'),('cartridge',' Cartridge'),('de_replacement',' DE Replacement')],'Filter Type',default='media_type')
    media_type = fields.Selection([('glass','Glass'),('sand','Sand'),('zeolite','Zeolite')],'Media Type')
    filter_product_brand_id = fields.Many2one('product.brand',string='Product Brand')
    filter_product_id = fields.Many2one('product.product',string='Product')
    filter_installation_date = fields.Date(string='Filter Installation Date')
    filter_notes = fields.Text(string='Notes')
    heat_type = fields.Boolean(string='Pool Heating')
    heat_selection = fields.Selection(Heater_Type,string='Heat Type')
    heat_product_brand_id = fields.Many2one('product.brand',string='Product Brand')
    heat_product_id = fields.Many2one('product.product',string='Product')
    heat_installation_date = fields.Date(string='Installation Date')
    heat_notes = fields.Text(string='Notes')
    cleaner_type=fields.Boolean(string='Pool Cleaner')
    cleaner_selection=fields.Selection(Cleaner_Type,string='Cleaner Type')
    cleaner_product_brand_id = fields.Many2one('product.brand',string='Product Brand')
    cleaner_product_id = fields.Many2one('product.product',string='Product')
    cleaner_installation_date = fields.Date(string='Installation Date')
    cleaner_notes = fields.Text(string='Notes')
    light_type=fields.Boolean(string='Pool Lighting')
    light_product_brand_id = fields.Many2one('product.brand',string='Product Brand')
    light_product_id = fields.Many2one('product.product',string='Product')
    light_installation_date = fields.Date(string='Installation Date')
    light_notes = fields.Text(string='Notes')
    salt_level = fields.Float(string='Optimum Salt Level')
    min_salt_level = fields.Float(string='Min Salt Level')
    max_salt_level = fields.Float(string='Max Salt Level')
    skimmer_brand_id = fields.Many2one('product.brand',string='Product Brnad')
    skimmer_product_id = fields.Many2one('product.product',string='Product')
    skimmer_installation_date = fields.Date(string='Installation Date')
    skimmer_notes = fields.Text(string='Notes')
    
    @api.onchange('sanitising_product_id')
    def onchange_sanitising_product(self):
        self.salt_level = self.sanitising_product_id.salt_level
        
    
    
    
    
    
    
    
    
    
