from openerp import api, fields, models, _

class get_incoming_outgoing_picking(models.Model):
    _inherit = 'purchase.order'
    
    picking_ids = fields.One2many('stock.picking',compute='_get_picking_ids', string='Picking List', help="This is the list of receipts that have been generated for this purchase order.")
    
    @api.multi
    def _get_picking_ids(self):
        res = {}
        for po_id in self.ids:
            res[po_id] = []
        query = """
        SELECT picking_id, po.id FROM stock_picking p, stock_move m, purchase_order_line pol, purchase_order po
            WHERE po.id in %s and po.id = pol.order_id and pol.id = m.purchase_line_id and m.picking_id = p.id
            GROUP BY picking_id, po.id
             
        """
        self._cr.execute(query, (tuple(self.ids), ))
        picks = self._cr.fetchall()
        for pick_id, po_id in picks:
            self._cr.execute("""select distinct picking_id from stock_move where origin_returned_move_id in (
                select id from stock_move where picking_id = %s) """ %(pick_id))
            return_picking_ids = self._cr.fetchall()
            if return_picking_ids and return_picking_ids[0]:
                for return_pick in return_picking_ids:
                    res[po_id].append(return_pick[0])
            res[po_id].append(pick_id)
        #return res
        for purchase_id in self:
            pick_ids = res.get(purchase_id.id)
            purchase_id.picking_ids = pick_ids
              