from openerp import models, fields, _

class add_customer_tag(models.Model):
    _name = 'add.customer.tag'
    
    partner_id = fields.Many2one('res.partner',string="Customer")
    category_id = fields.Many2one('res.partner.category',string="Category")
    partner_site_id = fields.Many2one('partner.site',string="Partner Site")
    