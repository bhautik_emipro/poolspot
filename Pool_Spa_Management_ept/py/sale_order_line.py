from openerp import api, fields, models, _

class sale_order_line(models.Model):
    _inherit = 'sale.order.line'
     
    job_id = fields.Many2one('service.order.jobs',string="Job Id")
    

class sale_order(models.Model):
    _inherit = 'sale.order'
 
    job_ids = fields.One2many('service.order.jobs','sale_order',string="Jobs")
    