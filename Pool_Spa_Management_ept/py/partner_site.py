from openerp import api, fields, models, _
from pychart.arrow import default

class partner_site(models.Model):
     
    _name = 'partner.site'
    _description = 'Partner Site'
    _rec_name = 'customer_id'
     
    customer_id = fields.Many2one('res.partner',string='Customer')
    cal_service_id = fields.Many2one('calendar.service',string='Calendar Service')
    address = fields.Char(string='Address')
    
    city_id = fields.Many2one('res.country.state.city',string='City')
    state_id = fields.Many2one('res.country.state',string='State')
    zipcode = fields.Char(string='PostCode')
    country_id = fields.Many2one('res.country',string='Country')
    active = fields.Boolean(string='Active')
#     is_rental = fields.Selection([('yes','Yes'),('no','No')],'Is_Rental',default='no')
#     rental = fields.Selection([('real_estate_agency','Real Estate Agency (Partner)'),('property_manager','Property Manager (Partner)'),('primary_tenant','Primary Tenant (Partner)'),('secondary_tenant','Secondary Tenant (Partner)'),('other_contact','Other contact / tenant (PartneIs Renatl')],'Partner')
    category_ids = fields.Many2many('res.partner.category',string="Category")
#     partner_ids = fields.Many2many('res.partner',string="Customer")
    pool_site_ids = fields.Many2many('pool.detail',string='Pool-Site Name')
    notes = fields.Text(string='Notes')
    key_require = fields.Boolean(string='Key Required')
    key_number = fields.Char(string='Key Number')
    gate_code = fields.Boolean(string='Gate Code')
    gate_number = fields.Char(string='Gate Number')
    dogs = fields.Boolean(string='Dogs')
    customer_tag_ids = fields.One2many('add.customer.tag','partner_site_id',string='Customer')
    
    @api.onchange('category_ids')
    def onchange_category(self):
        """ Search the Partner those category_id in res.partner  match in res.partner.category """
        partners = self.env['res.partner'].search([('category_id','in',self.category_ids.ids)])
        """ return the partner whose category_id match """
        return {'domain':{'partner_ids':[('id','in',partners.ids)]}}
    
    @api.onchange('customer_id')    
    def onchange_partner(self):
        city = self.customer_id.city
        city_id = self.env['res.country.state.city'].search([('name','=',city)])
        self.city_id = city_id.id
        self.address = self.customer_id.address_description
        self.state_id = self.customer_id.state_id
        self.country_id = self.customer_id.country_id
        self.zipcode = self.customer_id.zip
        
        
        
