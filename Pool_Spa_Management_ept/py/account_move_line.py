from openerp import models, _

class account_move_line(models.Model):
    _inherit = 'res.partner'
    
    # Method Get From the account_invoice.py File    
    
    def _find_accounting_partner(self, partner):
        '''
        Find the partner for which the accounting entries will be created
        '''
        return partner
    
    