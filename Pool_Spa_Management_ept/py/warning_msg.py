from openerp import models, fields, api,_
from openerp.exceptions import except_orm

class warning_msg(models.TransientModel):
    _name = 'warning.msg'
    
    service_id = fields.Many2one('calendar.service',string="service id")
    
    #===========================================================================================
    # When Press the Done Button on Service Order Check the All the Job are in done state if not  
    # Show the Warning message if press yes create the sale order of job whose state is done.add 
    # product in product page.
    #===========================================================================================
    
    @api.multi
    def action_done(self):
        if self.service_id.work_type == 'wait':
            raise Warning(_("Service with Waiting type can't be closed!"))
                    
         
            
        cal_serv_cal = self.env['calendar.service.calendar']   
             
        # avoid forcing default_state values when creating sale orders
        order_obj = self.env['sale.order'].with_context({
        key: val
        for key, val in self.env.context.iteritems()
        if not (isinstance(key, basestring) and key == 'default_state')
        })
        line_obj = self.env['sale.order.line'].with_context({
        key: val
        for key, val in self.env.context.iteritems()
        if not (isinstance(key, basestring) and key == 'default_state')                
        })
     
        vals = {'partner_id': self.service_id.partner_id.id,'date_order': self.service_id.start_time,
        'pricelist_id': self.service_id.partner_id.property_product_pricelist.id,
        'user_id': self.service_id.user_id.id, 'calendar_service_id': self.service_id.id,
        }
        order = order_obj.create(vals)
        qty = 1.0
         

         
         
        if self.service_id.service_product_ids:    
         
            for product_id in self.service_id.service_product_ids:              
                line_vals = {'product_id': product_id.product_id.id, 'product_uom_qty':product_id.qty, 'state': 'draft', 'order_id':order.id}
                result = line_obj.product_id_change_with_wh([], False, product_id.id, product_id.qty)
                line_obj.create(line_vals)
                 
        if self.service_id.order_id.state == 'draft':
            order = self.service_id.order_id.write(vals)
         
         
         
        if self.service_id.job_ids:
#             flag = 0
            for job in self.service_id.job_ids:
                if  job.state == 'done' and not job.sale_order and job.product_ids:
                    for product_id in job.product_ids:
                        line_vals = {'product_id':product_id.id,'job_id':job.id ,'product_uom_qty':qty, 'state': 'draft', 'order_id':order.id } 
                        line_obj.create(line_vals)
                        job.set_job = True
                    job.sale_order = order.id
                    self.service_id.order_id = order.id
                    self.env['service.order.jobs'].create_analytical_account(job)
                else :
                    self.service_id.order_id = order.id
        
        self.service_id.order_id = order.id
        
        self.service_id.state = 'done'
    
    