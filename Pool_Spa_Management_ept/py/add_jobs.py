from openerp import api, fields, models, _
from datetime import datetime, timedelta

class add_jobs(models.Model):
    _name = 'add.jobs'
    _rec_name = 'jobname'
    
    jobname = fields.Char(string='Job')
    
    