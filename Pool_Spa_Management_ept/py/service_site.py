from openerp import fields, models, api, _
from datetime import datetime, timedelta
from openerp.exceptions import except_orm

import base64
import werkzeug
from urlparse import urljoin
from urllib import urlencode

class service_site(models.Model):
    _inherit = 'mail.thread'
    _inherit = 'calendar.service'
    
    
    site_ids = fields.Many2many('partner.site', string='Site')
    
    job_ids = fields.One2many('service.order.jobs', 'calendar_service_id', string='Jobs')

    desc = fields.Text(string='Description')
    
    @api.model
    def create(self, vals):
        record = super(service_site, self).create(vals)
        if record.job_ids:
            for job_id in record.job_ids:
                msg = _('%s Job Created For Service Order %s') % (job_id.job_id.jobname,record.id)
                record.message_post(body=msg, subtype='mail.mt_comment',)
        return record
    
    
    #===========================================================================================
    # When Press the Done Button on Service Order Check the All the Job are in done state if not  
    # Show the Warning message if press yes create the sale order of job whose state is done.add 
    # product in product page.
    #===========================================================================================
    
    @api.multi
    def close_state(self):
        flag = 0
        if self.job_ids:
            
            
            for job in self.job_ids:
                if job.state != 'done':
                    flag = 1
                
            if flag == 1:
                return {
                    'name':_("Warning"),
                    'view_mode': 'form',                           
                    'view_type': 'form',
                    'res_model': 'warning.msg',
                    'type': 'ir.actions.act_window',
                    'target': 'new',
                    'context':{'default_service_id':self.id}
                    }
            else:
                if self.work_type == 'wait':
                    raise Warning(_("Service with Waiting type can't be closed!"))
        
                cal_serv_cal = self.env['calendar.service.calendar']   
                     
                # avoid forcing default_state values when creating sale orders
                order_obj = self.env['sale.order'].with_context({
                 key: val
                 for key, val in self.env.context.iteritems()
                 if not (isinstance(key, basestring) and key == 'default_state')
                 })
                line_obj = self.env['sale.order.line'].with_context({
                 key: val
                 for key, val in self.env.context.iteritems()
                 if not (isinstance(key, basestring) and key == 'default_state')                
                 })
             
                vals = {'partner_id': self.partner_id.id, 'date_order': self.start_time,
                 'pricelist_id': self.partner_id.property_product_pricelist.id,
                 'user_id': self.user_id.id, 'calendar_service_id': self.id,
                 }
                order = order_obj.create(vals)
                message = "Sale Order Created %s"%(order.id)
                self.message_post(body=message,subtype='mail.mt_comment')
                 
                qty = 1.0
                 
                for work in self.work_ids:  
                    start_time = cal_serv_cal.str_to_dt(work.start_time)
                #   end_time = cal_serv_cal.str_to_dt(work.end_time)
                #   qty += round(cal_serv_cal.get_duration(start_time, end_time), 3) #converting duration as qty in hours
                    line_vals = {'product_id': work.product_id.id, 'state': 'draft', 'order_id':order.id}
                    result = line_obj.product_id_change_with_wh([], False, work.product_id.id, qty)  # On Change
                    line_obj.create(line_vals)
                      
                 
                 
                if self.service_product_ids:    
                    for product_id in self.service_product_ids:              
                        line_vals = {'product_id': product_id.product_id.id, 'product_uom_qty':product_id.qty, 'state': 'draft', 'order_id':order.id}
                        result = line_obj.product_id_change_with_wh([], False, product_id.id, product_id.qty)
                        line_obj.create(line_vals)
                         
                if self.order_id.state == 'draft':
                    order = self.order_id.write(vals)
                
                
                for job in self.job_ids:
                    if not job.sale_order:
                        if job.product_ids:
                            for product_id in job.product_ids:
                                line_vals = {'product_id':product_id.id,'job_id':job.id ,'product_uom_qty':qty, 'state': 'draft', 'order_id':order.id } 
                                line_obj.create(line_vals)
                        
                        job.set_job = True
                        job.sale_order = order.id
                            
                        self.env['service.order.jobs'].create_analytical_account(job)
                
            
            self.order_id = order.id
                            
            self.state = 'done'

        else:
            if self.work_type == 'wait':
                raise Warning(_("Service with Waiting type can't be closed!"))
        
    
    
            cal_serv_cal = self.env['calendar.service.calendar']   
                 
            # avoid forcing default_state values when creating sale orders
            order_obj = self.env['sale.order'].with_context({
             key: val
             for key, val in self.env.context.iteritems()
             if not (isinstance(key, basestring) and key == 'default_state')
             })
            line_obj = self.env['sale.order.line'].with_context({
             key: val
             for key, val in self.env.context.iteritems()
             if not (isinstance(key, basestring) and key == 'default_state')                
             })
         
            vals = {'partner_id': self.partner_id.id, 'date_order': self.start_time,
             'pricelist_id': self.partner_id.property_product_pricelist.id,
             'user_id': self.user_id.id, 'calendar_service_id': self.id,
             }
            order = order_obj.create(vals)
            message = "Sale Order Created %s"%(order.id)
            self.message_post(body=message,subtype='mail.mt_comment')
             
            qty = 1.0
             
            for work in self.work_ids:  
                start_time = cal_serv_cal.str_to_dt(work.start_time)
            #   end_time = cal_serv_cal.str_to_dt(work.end_time)
            #   qty += round(cal_serv_cal.get_duration(start_time, end_time), 3) #converting duration as qty in hours
                line_vals = {'product_id': work.product_id.id, 'state': 'draft', 'order_id':order.id}
                result = line_obj.product_id_change_with_wh([], False, work.product_id.id, qty)  # On Change
                line_obj.create(line_vals)
                  
             
             
            if self.service_product_ids:    
                for product_id in self.service_product_ids:              
                    line_vals = {'product_id': product_id.product_id.id, 'product_uom_qty':product_id.qty, 'state': 'draft', 'order_id':order.id}
                    result = line_obj.product_id_change_with_wh([], False, product_id.id, product_id.qty)
                    line_obj.create(line_vals)
                     
            if self.order_id.state == 'draft':
                order = self.order_id.write(vals)
             
            self.order_id = order.id
                    
            self.state = 'done'
            
    #=========================================================================================
    # When User Delete the Done Service Order. Raise Warning .... 
    #
    #=========================================================================================
    
    @api.multi
    def unlink(self):
        for service in self:
            if service.state =='done':
                raise except_orm('Warning..!','You can not delete the done Service order..')
            
    
    
        
        
        
            