{
    'name': 'Pool-Spa Management',
    'version': '1.0',
    'category': 'Pool-Spa Management',
    'description': """
This is extention of Pool-Spa Management
""",
    'author': 'Emipro Technologies Pvt. Ltd.',
    'website': 'www.emiprotechnologies.com',
    'depends': [
        'base_location_extension','calendar_service_ept'
    ],
    'data': [
        'security/pool_spa_security.xml',
        'security/ir.model.access.csv',
        'wizard/sale_order_job_ept.xml',
        'xml/pool_surface.xml',
        'xml/enter_sanitising_method.xml',
        'xml/add_jobs.xml',
        'xml/service_order_jobs.xml',
        'xml/service_order_job_sequence.xml',
        'xml/site_service.xml',
        'xml/pool_surface.xml',
       	'xml/pool_profile.xml',
        'xml/menu.xml',
        'xml/partner_site.xml',
        'xml/pos_site_res_partner.xml',
        'xml/res_partner_change.xml',
	    'xml/product_template.xml',
        'xml/sale_order_job.xml',
        'report/service_order_report.xml',
        
     
        
    ],
    'installable': True,
    'application': True,
    'auto_install': False,
}
