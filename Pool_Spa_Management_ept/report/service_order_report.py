from openerp import api, fields, models, tools, _

class service_order_report(models.Model):
    _name = "service.order.report"
    _description = "Service Order Job Analysis"
    _auto = False
    
    user_id = fields.Many2one('res.users',string='Salesman',readonly=True)
    partner_id = fields.Many2one('res.partner', string='Partner', readonly=True)
    service_order_job_id = fields.Many2one('service.order.jobs',string='job', readonly=True)
    calendar_service_id = fields.Many2one('calendar.service',string='Service Order',readonly=True)
    jobname = fields.Many2one('add.jobs',string='Jobname')
    start_date = fields.Date(string="Date Order", readonly=True)
    state = fields.Selection(
        [('draft', 'Draft'), ('open', 'Open'), ('pending', 'Pending'),('done', 'Done')],
        string='Status',readonly=True)
    
    def init(self, cr):
        tools.drop_view_if_exists(cr, self._table)
        _query = """ create or replace view service_order_report  as(
                    select row_number() OVER() as id,
                    so.id as service_order_job_id ,
                    cal.id as calendar_service_id ,
                    so.user_id as user_id,
                    so.state as state,
                    cal.partner_id as partner_id,
                    cal.user_id as cal_user_id,
                    aj.id as jobname,
                    to_char(so.start_date::timestamp with time zone, 'YYYY-MM-DD'::text) as start_date
                    from service_order_jobs so inner join calendar_service cal
                    on (so.calendar_service_id=cal.id)
                    inner join add_jobs aj on (aj.id=so.job_id) 
                    group by 
                    so.id,
                    cal.id,
                    so.id,
                    so.user_id,
                    so.state,
                    so.start_date,
                    cal.partner_id,
                    cal.user_id,
                    aj.id
                    )
            """
        cr.execute(_query)
    
service_order_report()
    
    
    