# -*- coding: utf-8 -*-
##############################################################################
#
#    OpenERP, Open Source Management Solution
#    Copyright (c) 2013-Present Acespritech Solutions Pvt. Ltd. (<http://acespritech.com>).
#
#    This program is free software: you can redistribute it and/or modify
#    it under the terms of the GNU Affero General Public License as
#    published by the Free Software Foundation, either version 3 of the
#    License, or (at your option) any later version.
#
#    This program is distributed in the hope that it will be useful,
#    but WITHOUT ANY WARRANTY; without even the implied warranty of
#    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#    GNU Affero General Public License for more details.
#
#    You should have received a copy of the GNU Affero General Public License
#    along with this program.  If not, see <http://www.gnu.org/licenses/>.
#
##############################################################################
from openerp import fields, models, api, _
import base64
from wand.image import Image
import os, glob
import os.path
from openerp.exceptions import except_orm, Warning, RedirectWarning

paper_format_dict = {'A0':[841 ,1189],'A1' :[594, 841], 'A2' : [420, 594], 'A3' :[297,420], 'A4' : [210, 297],
                     'A5' :[148, 210], 'A6' :[105, 148], 'A7' :[74, 105], 'A8' : [52, 74], 'A9' : [37, 52],
                     'B0' : [1000, 1414], 'B1' :[707 ,1000], 'B2' : [500, 707], 'B3' : [353, 500], 'B4' :[250, 353],
                    'B5' : [176, 250], 'B6' :[125, 176],'B7' : [88, 125], 'B8' :[62, 88],'B9' :[33, 62], 'B10' : [31, 44],
                    'C5E' : [163, 229], 'Comm10E' :[105, 241], 'DLE' : [110, 220],'Executive' :[190.5, 254],
                    'Folio' :[210, 330], 'Ledger' :[431.8, 279.4], 'Legal' : [215.9, 355.6],'Letter' :[215.9, 279.4],
                    'Tabloid' :[279.4, 431.8]}


class product_label_design(models.Model):
    _name = 'product.label.design'

    @api.model
    def default_get(self, fields_list):
        prod_list = []
        res = super(product_label_design, self).default_get(fields_list)
        if self._context.get('wiz_id') and self._context.get('from_wizard'):
            for wiz in self.env['wizard.product.report'].browse(self._context.get('wiz_id')):
                res.update({'template_design' : wiz.report_design if wiz.report_mode =='label' else '',
                            'page_template_design' : wiz.column_report_design if wiz.report_mode =='page' else '',
                            'page_width' : wiz.page_width, 'report_mode' : wiz.report_mode,
                            'page_height' : wiz.page_height, 'dpi' : wiz.dpi,
                            'margin_top' : wiz.margin_top, 'margin_left' : wiz.margin_left,
                            'margin_bottom' : wiz.margin_bottom, 'margin_right' : wiz.margin_right,
                            'orientation' : wiz.orientation, 'barcode_type' : wiz.barcode_type,
                            'humanReadable' : wiz.humanReadable, 'barcode_height' : wiz.barcode_height,
                            'barcode_width' : wiz.barcode_width, 'display_height' : wiz.display_height,
                            'display_width' : wiz.display_width, 'with_barcode' : wiz.with_barcode,
                            'format' : wiz.format, 'col_no' : wiz.col_no,
                            'col_width' : wiz.col_width, 'col_height' : wiz.col_height})
        return res

    name = fields.Char(string="Design Name")
    template_design = fields.Text(string="Template Design")
    page_template_design = fields.Text(string="Report Design")
    #page
    page_width = fields.Integer(string='Page Width (mm)', default=43)
    page_height = fields.Integer(string='Page Height (mm)', default=30)
    dpi = fields.Integer(string='DPI', default=80, help="The number of individual dots\
                                that can be placed in a line within the span of 1 inch (2.54 cm)")
    margin_top = fields.Integer(string='Margin Top (mm)', default=4)
    margin_left = fields.Integer(string='Margin Left (mm)', default=1)
    margin_bottom =  fields.Integer(string='Margin Bottom (mm)', default=1)
    margin_right = fields.Integer(string='Margin Right (mm)', default=1)
    orientation = fields.Selection([('Landscape', 'Landscape'),
                                    ('Portrait', 'Portrait')], 
                                   string='Orientation', default='Portrait', required=True)
    #barcode
    barcode_type = fields.Selection([('Codabar', 'Codabar'),('Code11', 'Code11'),
                                     ('Code128', 'Code128'),('EAN13', 'EAN13'),
                                     ('Extended39', 'Extended39'),('EAN8', 'EAN8'),
                                     ('Extended93', 'Extended93'),('USPS_4State', 'USPS_4State'),
                                     ('I2of5', 'I2of5'),('UPCA', 'UPCA'),
                                     ('QR', 'QR')],
                                    string='Type', default='EAN13', required=True)
    humanReadable = fields.Boolean(string="HumanReadable", help="User wants to print barcode number\
                                    with barcode label.")
    barcode_height = fields.Integer(string="Height", default=300, required=True, help="This height will\
                                    required for the clearity of the barcode.")
    barcode_width = fields.Integer(string="Width", default=1500, required=True, help="This width will \
                                    required for the clearity of the barcode.")
    display_height = fields.Integer(string="Display Height (px)", required=True, default=30, 
                                    help="This height will required for display barcode in label.")
    display_width = fields.Integer(string="Display Width (px)", required=True, default=120, 
                                   help="This width will required for display barcode in label.")
    with_barcode =  fields.Boolean(string='Barcode', help="Click this check box if user want to print\
                                    barcode for Product Label.", default=True)
    #new columns and rows fields
    format = fields.Selection([('A0', 'A0  5   841 x 1189 mm'),
                                ('A1', 'A1  6   594 x 841 mm'),
                                ('A2', 'A2  7   420 x 594 mm'),
                                ('A3', 'A3  8   297 x 420 mm'),
                                ('A4', 'A4  0   210 x 297 mm, 8.26 x 11.69 inches'),
                                ('A5', 'A5  9   148 x 210 mm'),
                                ('A6', 'A6  10  105 x 148 mm'),
                                ('A7', 'A7  11  74 x 105 mm'),
                                ('A8', 'A8  12  52 x 74 mm'),
                                ('A9', 'A9  13  37 x 52 mm'),
                                ('B0', 'B0  14  1000 x 1414 mm'),
                                ('B1', 'B1  15  707 x 1000 mm'),
                                ('B2', 'B2  17  500 x 707 mm'),
                                ('B3', 'B3  18  353 x 500 mm'),
                                ('B4', 'B4  19  250 x 353 mm'),
                                ('B5', 'B5  1   176 x 250 mm, 6.93 x 9.84 inches'),
                                ('B6', 'B6  20  125 x 176 mm'),
                                ('B7', 'B7  21  88 x 125 mm'),
                                ('B8', 'B8  22  62 x 88 mm'),
                                ('B9', 'B9  23  33 x 62 mm'),
                                ('B10', ':B10    16  31 x 44 mm'),
                                ('C5E', 'C5E 24  163 x 229 mm'),
                                ('Comm10E', 'Comm10E 25  105 x 241 mm, U.S. '
                                 'Common 10 Envelope'),
                                ('DLE', 'DLE 26 110 x 220 mm'),
                                ('Executive', 'Executive 4   7.5 x 10 inches, '
                                 '190.5 x 254 mm'),
                                ('Folio', 'Folio 27  210 x 330 mm'),
                                ('Ledger', 'Ledger  28  431.8 x 279.4 mm'),
                                ('Legal', 'Legal    3   8.5 x 14 inches, '
                                 '215.9 x 355.6 mm'),
                                ('Letter', 'Letter 2 8.5 x 11 inches, '
                                 '215.9 x 279.4 mm'),
                                ('Tabloid', 'Tabloid 29 279.4 x 431.8 mm'),
                                ('custom', 'Custom')],
                               string='Paper Type', default="custom",
                               help="Select Proper Paper size")
    col_no = fields.Integer('No. of Column', default=1)
    col_width = fields.Float('Column Width (mm)', default=3)
    col_height = fields.Float('Column Height (mm)', default=3)
    from_col = fields.Integer(string="Start Column", default=1)
    from_row = fields.Integer(string="Start Row", default=1)
    column_report_design = fields.Text(string="Report Design")
    report_mode = fields.Selection([('label','Label'), ('page','Page')],
                                  string="Report Mode", default="label")
    active = fields.Boolean(string="Active", default=True)

    @api.multi
    def close_wizard(self):
        res_id = self._context.get('wiz_id')
        self.write({'active' : False})
        return {
            'name': _('Print Product Label'),
            'type': 'ir.actions.act_window',
            'view_mode': 'form',
            'res_model': 'wizard.product.report',
            'target': 'new',
            'res_id': res_id ,
            'context': self.env.context,
        }

    @api.multi
    def go_to_label_wizard(self):
        res_id = self._context.get('wiz_id')
        if not self.name:
            raise Warning(_('Label Design Name is required.'))
        ctx = {}
        return {
            'name': _('Product Label'),
            'type': 'ir.actions.act_window',
            'view_mode': 'form',
            'res_model': 'wizard.product.report',
            'target': 'new',
            'res_id': res_id,
            'context' : ctx,
        }


class product_label_qty(models.TransientModel):
    _name='product.label.qty'

    @api.onchange('product_id')
    def onchange_product_id(self):
        if self.product_id:
            self.qty = 1

    product_id = fields.Many2one('product.product',string='Product', required=True)
    qty =  fields.Float(string='Quantity')
    prod_small_wiz_id = fields.Many2one('wizard.product.report', string='Product Wizard')


class wizard_product_report(models.TransientModel):
    _name = "wizard.product.report"

    @api.model
    def _get_report_design(self):
        view_obj = self.env['ir.ui.view']
        view_id = view_obj.search([('name', '=', 'product_label_report_template')])
        if view_id.arch:
            return view_id.arch
        return True

    @api.model
    def _get_page_report_design(self):
        view_obj = self.env['ir.ui.view']
        view_id = view_obj.search([('name', '=', 'product_page_report_temp')])
        if view_id.arch:
            return view_id.arch
        return True

    @api.model
    def _get_page_report_id(self):
        view_obj = self.env['ir.ui.view']
        view_id = view_obj.search([('name', '=', 'product_page_report_temp')])
        if view_id:
            return view_id
        else:
            raise RedirectWarning('Someone has deleted the reference view of report.\
                Please Update the module!')

    @api.model
    def _get_report_id(self):
        view_obj = self.env['ir.ui.view']
        view_id = view_obj.search([('name', '=', 'product_label_report_template')])
        if view_id:
            return view_id.id
        else:
            raise RedirectWarning('Someone has deleted the reference view of report.\
                Please Update the module!')

    @api.model
    def _get_report_paperformat_id(self):
        report_xml_obj = self.env['ir.actions.report.xml']
        xml_id = report_xml_obj.search([('report_name', '=', 'dynamic_product_label.product_label_report_template')])
        if xml_id and xml_id.paperformat_id:
            return xml_id.paperformat_id.id
        else:
            raise RedirectWarning('Someone has deleted the reference paperformat of report.\
                Please Update the module!')

    @api.onchange('paper_format_id')
    def onchange_report_paperformat_id(self):
        if self.paper_format_id:
            self.format = self.paper_format_id.format
            self.page_width = self.paper_format_id.page_width
            self.page_height = self.paper_format_id.page_height
            self.orientation = self.paper_format_id.orientation
            self.margin_top = self.paper_format_id.margin_top
            self.margin_left = self.paper_format_id.margin_left
            self.margin_bottom = self.paper_format_id.margin_bottom
            self.margin_right = self.paper_format_id.margin_right
            self.dpi = self.paper_format_id.dpi

    @api.onchange('dpi')
    def on_change_dpi(self):
        if self.dpi and self.dpi < 80:
            self.dpi = 80

    @api.onchange('design_id')
    def on_change_design_id(self):
        if self.design_id:
            self.report_mode = self.design_id.report_mode
            self.column_report_design = self.design_id.page_template_design
            self.report_design = self.design_id.template_design
            #paper format args
            self.format = self.design_id.format
            self.page_width = self.design_id.page_width
            self.page_height = self.design_id.page_height
            self.orientation = self.design_id.orientation
            self.dpi = self.design_id.dpi
            self.margin_top = self.design_id.margin_top
            self.margin_left = self.design_id.margin_left
            self.margin_bottom = self.design_id.margin_bottom
            self.margin_right = self.design_id.margin_right
            #barcode args
            self.with_barcode = self.design_id.with_barcode
            self.barcode_type = self.design_id.barcode_type
            self.barcode_height = self.design_id.barcode_height
            self.barcode_width = self.design_id.barcode_width
            self.humanReadable = self.design_id.humanReadable
            self.display_height = self.design_id.display_height
            self.display_width = self.design_id.display_width
            #display row col args
            self.col_no = self.design_id.col_no
            self.col_width = self.design_id.col_width
            self.col_height = self.design_id.col_height

    @api.multi
    @api.onchange('dpi')
    def onchange_dpi(self):
        if self.dpi < 80:
            self.dpi = 80

    @api.multi
    @api.onchange('col_width', 'col_height')
    def onchange_col_size(self):
        if self.col_height and self.col_width:
            if self.format != 'custom':
                format_size = paper_format_dict.get(self.format)
                if format_size:
                    total_col = format_size[0]/self.col_width
                    self.col_no = int(total_col)
                    self.col_no_float = total_col
            else:
                total_col = self.page_width/self.col_width
                self.col_no = int(total_col)
                self.col_no_float = total_col

    @api.multi
    @api.onchange('report_mode')
    def onchange_report_mode(self):
        if self.report_mode and (self.report_mode =='label'):
            self.format = 'custom'

    design_id = fields.Many2one('product.label.design', string="Template")
    product_ids = fields.One2many('product.label.qty', 'prod_small_wiz_id', string='Product List')
    page_width = fields.Integer(string='Page Width (mm)', default=43)
    page_height = fields.Integer(string='Page Height (mm)', default=30)
    dpi = fields.Integer(string='DPI', default=80, help="The number of individual dots \
                        that can be placed in a line within the span of 1 inch (2.54 cm)")
    margin_top = fields.Integer(string='Margin Top (mm)', default=4)
    margin_left = fields.Integer(string='Margin Left (mm)', default=1)
    margin_bottom =  fields.Integer(string='Margin Bottom (mm)', default=1)
    margin_right = fields.Integer(string='Margin Right (mm)', default=1)
    orientation = fields.Selection([('Landscape', 'Landscape'),
                                    ('Portrait', 'Portrait')], 
                                   string='Orientation', default='Portrait', required=True)
    #barcode input
    barcode_type = fields.Selection([('Codabar', 'Codabar'),('Code11', 'Code11'),
                                     ('Code128', 'Code128'),('EAN13', 'EAN13'),
                                     ('Extended39', 'Extended39'),('EAN8', 'EAN8'),
                                     ('Extended93', 'Extended93'),('USPS_4State', 'USPS_4State'),
                                     ('I2of5', 'I2of5'),('UPCA', 'UPCA'),
                                     ('QR', 'QR')],
                                    string='Type', default='EAN13', required=True)
    humanReadable = fields.Boolean(string="HumanReadable", help="User wants to print barcode number \
                                    with barcode label.")
    barcode_height = fields.Integer(string="Height", default=300, required=True, 
                                    help="This height will required for the clearity of the barcode.")
    barcode_width = fields.Integer(string="Width", default=1500, required=True, 
                                   help="This width will required for the clearity of the barcode.")
    display_height = fields.Integer(string="Display Height (px)", required=True, default=30, 
                                    help="This height will required for display barcode in label.")
    display_width = fields.Integer(string="Display Width (px)", required=True, default=120, 
                                   help="This width will required for display barcode in label.")
    #report design
    report_design = fields.Text(string="Report Design", default=_get_report_design)
    view_id = fields.Many2one('ir.ui.view', string='Report View', default=_get_report_id)
    paper_format_id = fields.Many2one('report.paperformat', string="Paper Format", default=_get_report_paperformat_id)
    with_barcode =  fields.Boolean(string='Barcode', help="Click this check box if user want to\
                        print barcode for Product Label.", default=True)
    label_preview = fields.Binary(string="Label Preview")
    view_preview = fields.Boolean(string="View Preview")
    #new columns and rows fields
    format = fields.Selection([('A0', 'A0  5   841 x 1189 mm'),
                                ('A1', 'A1  6   594 x 841 mm'),
                                ('A2', 'A2  7   420 x 594 mm'),
                                ('A3', 'A3  8   297 x 420 mm'),
                                ('A4', 'A4  0   210 x 297 mm, 8.26 x 11.69 inches'),
                                ('A5', 'A5  9   148 x 210 mm'),
                                ('A6', 'A6  10  105 x 148 mm'),
                                ('A7', 'A7  11  74 x 105 mm'),
                                ('A8', 'A8  12  52 x 74 mm'),
                                ('A9', 'A9  13  37 x 52 mm'),
                                ('B0', 'B0  14  1000 x 1414 mm'),
                                ('B1', 'B1  15  707 x 1000 mm'),
                                ('B2', 'B2  17  500 x 707 mm'),
                                ('B3', 'B3  18  353 x 500 mm'),
                                ('B4', 'B4  19  250 x 353 mm'),
                                ('B5', 'B5  1   176 x 250 mm, 6.93 x 9.84 inches'),
                                ('B6', 'B6  20  125 x 176 mm'),
                                ('B7', 'B7  21  88 x 125 mm'),
                                ('B8', 'B8  22  62 x 88 mm'),
                                ('B9', 'B9  23  33 x 62 mm'),
                                ('B10', ':B10    16  31 x 44 mm'),
                                ('C5E', 'C5E 24  163 x 229 mm'),
                                ('Comm10E', 'Comm10E 25  105 x 241 mm, U.S. '
                                 'Common 10 Envelope'),
                                ('DLE', 'DLE 26 110 x 220 mm'),
                                ('Executive', 'Executive 4   7.5 x 10 inches, '
                                 '190.5 x 254 mm'),
                                ('Folio', 'Folio 27  210 x 330 mm'),
                                ('Ledger', 'Ledger  28  431.8 x 279.4 mm'),
                                ('Legal', 'Legal    3   8.5 x 14 inches, '
                                 '215.9 x 355.6 mm'),
                                ('Letter', 'Letter 2 8.5 x 11 inches, '
                                 '215.9 x 279.4 mm'),
                                ('Tabloid', 'Tabloid 29 279.4 x 431.8 mm'),
                                ('custom', 'Custom')],
                               string='Paper Type', default="custom",
                               help="Select Proper Paper size")
    col_no = fields.Integer('No. of Column')
    col_no_float = fields.Float('No. of Column', readonly=True, help="Column Size without Rounding.")
    col_width = fields.Float('Column Width (mm)', default=3)
    col_height = fields.Float('Column Height (mm)', default=3)
    from_col = fields.Integer(string="Start Column", default=1)
    from_row = fields.Integer(string="Start Row", default=1)
    page_report_id = fields.Many2one('ir.ui.view', string='Page Report View', default=_get_page_report_id)
    column_report_design = fields.Text(string="Report Design", default=_get_page_report_design)
    report_mode = fields.Selection([('label','Label'), ('page','Page')],
                                  string="Report Mode", default="label")

    @api.multi
    def action_print(self):
        if not self.product_ids:
            raise RedirectWarning('Select any product first.!')
        for product in self.product_ids:
            if product.qty <= 0:
                 raise RedirectWarning('%s product label qty should be greater then 0.!' 
                            %(product.product_id.name))
            if not product.product_id.ean13 and self.with_barcode:
                 raise RedirectWarning(_('%s Product does not contain barcode number you trying to print!' 
                            %(product.product_id.name)))
        if (self.format == 'custom') and ((self.page_height <= 0) or (self.page_width <= 0)):
            raise Warning(_('You can not give page width and page height to zero(0).'))
        if (self.margin_top < 0) or (self.margin_left < 0) or \
            (self.margin_bottom < 0) or (self.margin_right < 0):
            raise RedirectWarning('Margin Value(s) for report can not be negative!')
        if (self.col_no <= 0) and (self.report_mode == 'page'):
            raise Warning(_('Minimun 1 column Required to print labels in page.'))
        if (self.report_mode == 'page'):
            if (self.col_height <= 0) or (self.col_width <= 0):
                raise Warning(_('Give proper hight and width for page column.'))
            if (self.from_col <= 0) or (self.from_row <=0):
                raise Warning(_('Start row and column position should be 1 or greater.'))
            if self.from_col > self.col_no:
                raise Warning(_('Start column position can not be greater than no. of column.'))
        data = self.read()[0]
        data.update({'label_preview' : False})
        datas = {
            'ids': self._ids,
            'model': 'wizard.product.report',
            'form': data,
        }
        ctx = {
            'dynamic_size' : True,
            'data' : data
        }
        if self.view_id and self.report_design:
            self.view_id.write({'arch':self.report_design})
        if self.page_report_id and self.column_report_design:
            self.page_report_id.write({'arch':self.column_report_design})
        if self.paper_format_id:
            if self.format == 'custom':
                result = self.paper_format_id.write({
                                        'format':self.format,
                                        'page_width' : self.page_width,
                                        'page_height' : self.page_height,
                                        'orientation' :self.orientation,
                                        'margin_top' : self.margin_top,
                                        'margin_left' :self.margin_left,
                                        'margin_bottom' : self.margin_bottom,
                                        'margin_right' : self.margin_right,
                                        'dpi' : self.dpi,
                                    })
            else:
                result = self.paper_format_id.write({
                                        'format':self.format,
                                        'page_width' : 0,
                                        'page_height' : 0,
                                        'orientation' :self.orientation,
                                        'margin_top' : self.margin_top,
                                        'margin_left' :self.margin_left,
                                        'margin_bottom' : self.margin_bottom,
                                        'margin_right' : self.margin_right,
                                        'dpi' : self.dpi,
                                    })
        if self.report_mode == 'label':
            return self.env['report'].get_action(self, 
                            'dynamic_product_label.product_label_report_template',data=datas,)
        else:
            return self.env['report'].get_action(self, 
                            'dynamic_product_label.product_page_report_temp',data=datas,)

    @api.multi
    def save_design(self):
        view_id = self.env['ir.model.data'].get_object_reference('dynamic_product_label',
                                                'wizard_label_design_form_view')[1]
        ctx = {'wiz_id' : self.id}
        return {
            'name' : _('Product Label Design'),
            'view_type': 'form',
            'view_mode': 'form',
            'res_model': 'product.label.design',
            'type': 'ir.actions.act_window',
            'target': 'new',
            'view_id':view_id,
            'context' : ctx,
            'nodestroy': True,
        }

    @api.multi
    def action_preview(self):
        encoded_string = ''
        if len(self.product_ids) <= 0:
            raise Warning(_('Define product with at least 1 quantity for pdf preview.'))
        data = self.read()[0]
        if self.view_id:
            self.view_id.write({'arch':self.report_design})
        if self.paper_format_id:
            if self.report_mode == 'label':
                result = self.paper_format_id.write({
                                        'format':self.format,
                                        'page_width' : self.page_width,
                                        'page_height' : self.page_height,
                                        'orientation' :self.orientation,
                                        'margin_top' : self.margin_top,
                                        'margin_left' :self.margin_left,
                                        'margin_bottom' : self.margin_bottom,
                                        'margin_right' : self.margin_right,
                                        'dpi' : self.dpi,
                                    })
        if data.get('product_ids'):
            data['product_ids'] = [data.get('product_ids')[0]]
        data.update({'label_preview' : True})
        datas = {
            'ids': self._ids,
            'model': 'wizard.product.report',
            'form': data,
        }
        ctx = {
            'dynamic_size' : True,
            'data' : data,
        }
        report_name = 'dynamic_product_label.product_label_report_template'
        pdf_data = self.env['report'].get_html(self, report_name, data=datas)
        body = [(self.id, pdf_data)]
        pdf_image = self.env['report']._run_wkhtmltopdf([],[], body, None, self.paper_format_id,
                                                {}, {'loaded_documents': {}, 'model': u'product.product'})
        with Image(blob=pdf_image) as img:
            filelist = glob.glob("/tmp/*.jpg")
            for f in filelist:
                os.remove(f)
            img.save(filename="/tmp/temp.jpg")
        if os.path.exists("/tmp/temp-0.jpg"):
               with open(("/tmp/temp-0.jpg"), "rb") as image_file:
                   encoded_string = base64.b64encode(image_file.read())
        elif os.path.exists("/tmp/temp.jpg"):
            with open(("/tmp/temp.jpg"), "rb") as image_file:
                   encoded_string = base64.b64encode(image_file.read())
        self.write({'view_preview': True,'label_preview' : encoded_string})
        return {
            'name': _('Product Label'), 
            'type': 'ir.actions.act_window',
            'view_mode': 'form',
            'res_model': 'wizard.product.report',
            'target': 'new',
            'res_id': self.id ,
        }

# vim:expandtab:smartindent:tabstop=4:softtabstop=4:shiftwidth=4: