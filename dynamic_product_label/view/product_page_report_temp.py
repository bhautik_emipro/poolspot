# -*- coding: utf-8 -*-
##############################################################################
#
#    OpenERP, Open Source Management Solution
#    Copyright (c) 2013-Present Acespritech Solutions Pvt. Ltd. (<http://acespritech.com>).
#
#    This program is free software: you can redistribute it and/or modify
#    it under the terms of the GNU Affero General Public License as
#    published by the Free Software Foundation, either version 3 of the
#    License, or (at your option) any later version.
#
#    This program is distributed in the hope that it will be useful,
#    but WITHOUT ANY WARRANTY; without even the implied warranty of
#    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#    GNU Affero General Public License for more details.
#
#    You should have received a copy of the GNU Affero General Public License
#    along with this program.  If not, see <http://www.gnu.org/licenses/>.
#
##############################################################################

from openerp import models, fields, api, _
from reportlab.graphics.barcode import createBarcodeDrawing
from reportlab.graphics.shapes import Drawing
from base64 import b64encode
import math
from reportlab.graphics import barcode

class product_page_report_temp(models.AbstractModel):
    _name = 'report.dynamic_product_label.product_page_report_temp'

    @api.multi
    def render_html(self, data=None):
        report_obj = self.env['report']
        report = report_obj._get_report_from_name('dynamic_product_label.product_page_report_temp')
        docargs = {
            'doc_ids': self.env["wizard.product.report"].browse(data["ids"]),
            'doc_model': report.model,
            'docs': self,
            'draw_table' : self._draw_table,
            'get_barcode' : self._get_barcode,
            'draw_style' : self._draw_style,
            'data': data,
        }
        return report_obj.render('dynamic_product_label.product_page_report_temp', docargs)

    def _draw_style(self, data):
        width = str(data['form']['col_width'])
        height = str(data['form']['col_height'])
        style =  'width:'+width+'mm;height:'+height+'mm;margin:0px;padding:0px;overflow: hidden;text-overflow: ellipsis;'
        return style

    def get_cell_number(self, from_row, from_col, col_no):
        cell_no = ((from_row - 1) * col_no + from_col)
        return cell_no

    def _draw_table(self, data):
        product_list = []
        product_ids = data['form']['product_ids']
        from_row = data['form']['from_row']
        from_col = data['form']['from_col']
        col_no = data['form']['col_no']
        if product_ids:
            box_needed = 0
            produ_obj = self.env['product.label.qty']
            product_obj = self.env['product.product']
            cell_no = self.get_cell_number(from_row, from_col, int(col_no))
            box_needed = int(cell_no)
            for product_data in produ_obj.browse(product_ids):
                if product_data.product_id:
                        box_needed += int(product_data.qty)
                        product_list.append(product_data)
            cell_record = self.create_list(product_list, int(cell_no))
            _table =  self.create_table(box_needed, cell_record, data)
            return _table

    def _get_barcode(self, ean13, data):
        barcode_str = ''
        if data['form']['with_barcode']:
            width = int(data['form']['barcode_height'])
            height = int(data['form']['barcode_width'])
            barcode_str = barcode.createBarcodeDrawing(
                                data['form']['barcode_type'], value=ean13, format='png', width=width,
                                height=height, humanReadable = data['form']['humanReadable'])
            barcode_str = barcode_str.asString('png')
            encoded_string = b64encode(barcode_str)
            barcode_str = "<img style='width:"+str(data['form']['display_width'])+"px;height:"+str(data['form']['display_height'])+"px;' src=data:image/png;base64,{0}".format(encoded_string)+"</img>"
        return barcode_str

    def create_list(self, products, cell_no):
        product_data = {}
        for prod in products:
            if prod.product_id:
                for qty in range(0,int(prod.qty)):
                    product_data.update({cell_no : {'product_id': prod.product_id}})
                    cell_no+=1
        return product_data

    def create_table(self, box_needed, cell_record, data):
        no_of_col = int(data['form']['col_no'])
        col_width = str(data['form']['col_width'])+'mm'
        col_height =  str(data['form']['col_height'])+'mm'
        product_table = []
        for tr_no in range(1,int(math.ceil(float(box_needed)/no_of_col+1))):
            product_dict = {}
            for td_no in range(1,no_of_col+1):
                cellno = self.get_cell_number(tr_no, td_no, no_of_col)
                if cell_record.has_key(cellno):
                    product_id = cell_record.get(cellno).get('product_id')
                    if product_dict.has_key(tr_no):
                        product_dict[tr_no].append(product_id)
                    else:
                        product_dict[tr_no] = [product_id]
                else:
                    if product_dict.has_key(tr_no):
                        product_dict[tr_no].append(False)
                    else:
                        product_dict[tr_no] = [False]
            product_table.append(product_dict)
        return product_table


# vim:expandtab:smartindent:tabstop=4:softtabstop=4:shiftwidth=4:
