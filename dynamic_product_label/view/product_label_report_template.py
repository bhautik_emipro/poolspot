# -*- coding: utf-8 -*-
##############################################################################
#
#    OpenERP, Open Source Management Solution
#    Copyright (c) 2013-Present Acespritech Solutions Pvt. Ltd. (<http://acespritech.com>).
#
#    This program is free software: you can redistribute it and/or modify
#    it under the terms of the GNU Affero General Public License as
#    published by the Free Software Foundation, either version 3 of the
#    License, or (at your option) any later version.
#
#    This program is distributed in the hope that it will be useful,
#    but WITHOUT ANY WARRANTY; without even the implied warranty of
#    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#    GNU Affero General Public License for more details.
#
#    You should have received a copy of the GNU Affero General Public License
#    along with this program.  If not, see <http://www.gnu.org/licenses/>.
#
##############################################################################

from openerp import models, fields, api, _
from reportlab.graphics import barcode 
from base64 import b64encode
from openerp.exceptions import RedirectWarning


class product_label_report_template(models.AbstractModel):
    _name = 'report.dynamic_product_label.product_label_report_template'

    @api.multi
    def render_html(self, data=None):
        report_obj = self.env['report']
        report = report_obj._get_report_from_name('dynamic_product_label.product_label_report_template')
        docargs = {
            'doc_ids': self.env["wizard.product.report"].browse(data["ids"]),
            'doc_model': report.model,
            'docs': self,
            'get_barcode_data' : self._get_barcode_data,
            'get_barcode_string' : self._get_barcode_string,
            'data': data,
        }
        if data['form']['with_barcode']:
            for product in self.env['product.label.qty'].browse(data['form']['product_ids']):
                width = int(data['form']['barcode_height'])
                height = int(data['form']['barcode_width'])
                try:
                    barcode_str = barcode.createBarcodeDrawing(
                                    data['form']['barcode_type'], value=product.product_id.ean13, format='png', width=width, height=height,
                                    humanReadable = data['form']['humanReadable'])
                except:
                    raise RedirectWarning('Select valid barcode type according product ean13 value !')
        return report_obj.render('dynamic_product_label.product_label_report_template', docargs)

    def _get_barcode_string(self, ean13, data):
        barcode_str = ''
        if data['form']['with_barcode']:
            width = int(data['form']['barcode_height'])
            height = int(data['form']['barcode_width'])
            barcode_str = barcode.createBarcodeDrawing(
                                data['form']['barcode_type'], value=ean13, format='png', width=width,
                                height=height, humanReadable = data['form']['humanReadable'])
            barcode_str = barcode_str.asString('png')
            encoded_string = b64encode(barcode_str)
            barcode_str = "<img style='width:"+str(data['form']['display_width'])+"px;height:"+str(data['form']['display_height'])+"px' src='data:image/png;base64,{0}'>".format(encoded_string)
        return barcode_str

    def _get_barcode_data(self, data):
        product_list =[]
        product_ids = self.env['product.label.qty'].search([('id', 'in', data['form']['product_ids'])])
        if data.get('form').get('label_preview'):
            product_list.append(product_ids[0].product_id)
            return product_list
        else:
            for product in product_ids:
                for qty in range(int(product.qty)):
                    product_list.append(product.product_id)
        return product_list


# vim:expandtab:smartindent:tabstop=4:softtabstop=4:shiftwidth=4: