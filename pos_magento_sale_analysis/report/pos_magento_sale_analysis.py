from openerp import api, fields, models, tools, _


class pos_magento_sale_analysis(models.Model):
    _name = "pos.magento.sale.analysis"
    _description = "Pos Magento Sale Analysis Report"
    _auto = False
     
    pos = fields.Boolean(string="Pos")
    product_id = fields.Many2one('product.product', string='Product', readonly=True)
    partner_id = fields.Many2one('res.partner', string='Partner', readonly=True)
    magento_id = fields.Many2one('magento.sale.order',string="Magento id",readonly=True)
    date_order = fields.Datetime(string='Date Order', readonly=True)
    sum = fields.Float(string="Total Sale",readonly=True)
    year = fields.Char(string="year",readonly=True)
    month = fields.Selection([('01', 'January'), ('02', 'February'), ('03', 'March'), ('04', 'April'),
                                ('05', 'May'), ('06', 'June'), ('07', 'July'), ('08', 'August'), ('09', 'September'),
                                ('10', 'October'), ('11', 'November'), ('12', 'December')], string='Order Month', readonly=True)
    day = fields.Char(string="Day", readonly=True)
    sale_channel = fields.Selection(
        [('sale_order', 'Sale-Order'), ('magento', 'Ecommerce-Order'), ('pos_order', 'Pos-Order')],
        string='Status',readonly=True)
    
    
    def init(self, cr):
        tools.drop_view_if_exists(cr, self._table)
        _query = """ create or replace view pos_magento_sale_analysis as(
                    select sol.id, 
                    false as  pos,
                    sol.product_id, 
                    so.partner_id, 
                    mso.magento_id as magento_id, 
                    so.date_order, 
                    sum(sol.price_unit * sol.product_uom_qty),
                    to_char(so.date_order::timestamp with time zone, 'YYYY'::text) AS year,
                    to_char(so.date_order::timestamp with time zone, 'MM'::text) AS month,
                    to_char(so.date_order::timestamp with time zone, 'YYYY-MM-DD'::text) AS day,
                    CASE WHEN mso.openerp_id is not null then 'magento' 
                    ELSE 'sale_order' 
                    END as sale_channel
                    from sale_order so join sale_order_line sol on sol.order_id = so.id
                    left join magento_sale_order mso on mso.openerp_id = so.id
                    group by sol.id, so.id, sol.product_id, so.partner_id, mso.magento_id, so.date_order, mso.openerp_id
                    UNION ALL(
                    select line.id, 
                    true as pos, 
                    line.product_id, 
                    pos.partner_id, 
                    null as magento_id, 
                    pos.date_order, 
                    sum(price_subtotal),
                    to_char(pos.date_order::timestamp with time zone, 'YYYY'::text) AS year,
                    to_char(pos.date_order::timestamp with time zone, 'MM'::text) AS month,
                    to_char(pos.date_order::timestamp with time zone, 'YYYY-MM-DD'::text) AS day,
                    'pos_order' as sale_channel
                    from pos_order 
                    pos join pos_order_line line on line.order_id = pos.id
                    group by line.id, pos.id, line.product_id, pos.partner_id, pos.date_order)
                 )
        """
        cr.execute(_query)
    
pos_magento_sale_analysis()
        
        
        
