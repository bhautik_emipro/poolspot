{
    'name': 'Pos-Magento-Sale-Analysis Report',
    'version': '1.0',
    'category': 'Analysis Report',
    'description': """
This is Analysis Report Of Pos-Magento Sale
""",
    'author': 'Emipro Technologies Pvt. Ltd.',
    'website': 'www.emiprotechnologies.com',
    'depends': [
        
    ],
    'data': [
      'security/ir.model.access.csv',  
      'view/pos_magento_sale_analysis.xml'
    ],
    'installable': True,
    'application': True,
    'auto_install': False,
}
