{ 

"name" : "Invoice Reopen",
"version" : "8.1",
"author" : "Emipro Technologies",
"category": 'Accounting & Finance',
'complexity': "normal",
"description": """
Allows reopeing of unpaid invoices.
=====================================

This module allows to reopen (set to draft) unpaid invoices.
To comply with good accounting practice the existing posted move associated
with the invoice will be renamed (add [YYMMDD HHMISS]) and a new move (same name ending with *)
will be created with reversed debit/credit.
The renamed and new lines are reconciled.
If the invoice is printed AND stored as attachment the attachment will be renamed too.
This allows to change all content of the invoice if necessary.

""",
'website': 'http://www.emiprotechnologies.com',
"depends" : ["account"],
'init_xml': [],
'data': ['view/account_view.xml'],
'demo_xml': [],
'installable': True,
'auto_install': False,
}

