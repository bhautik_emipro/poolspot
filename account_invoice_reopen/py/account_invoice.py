from openerp import fields,models,api,_
import time
from openerp import netsvc
from openerp.exceptions import Warning


class account_invoice(models.Model):
    _inherit = "account.invoice"

    @api.multi
    def allow_reopen(self):
        for invoice in self:
            if invoice.state == 'cancel':
                continue
            for move_line in invoice.payment_ids:
                if move_line.reconcile_id or (move_line.reconcile_partial_id and move_line.reconcile_partial_id.line_partial_ids):
                    raise Warning(_('Error !'), _('You can not reopen an invoice which is partially paid! You need to unreconcile related payment entries first!'))            
            for move in invoice.move_id:
                if not move.journal_id.reopen_posted:
                    raise Warning(_('Error !'), _('You can not reopen invoice of this journal [%s]! You need to need to set "Allow Update Posted Entries" first')%(move.journal_id.name))

        return True
    @api.multi
    def action_reopen(self):

        self.allow_reopen()

        context = {} 
        attachment_obj = self.env['ir.attachment']


        move_ids = [] 
        now = ' ' + _('Invalid') + time.strftime(' [%Y%m%d %H%M%S]')


        for invoice in self:
            if invoice.state == 'cancel':
                continue
            invoice.signal_workflow('invoice_cancel')

            attachments = attachment_obj.search([('res_model','=','account.invoice'),('res_id','=',invoice.id)])
            for attachment in attachments:
                vals = {
                    'name': attachment.name.replace('.pdf', now+'.pdf'),
                    'datas_fname': attachment.datas_fname.replace('.pdf.pdf', now+'.pdf.pdf')
                       }
                attachment.write(vals)

    @api.multi
    def action_number(self):
        for invoice in self:
            if not invoice.internal_number:
                super(account_invoice, self).action_number()
        return True
