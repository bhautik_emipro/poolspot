from openerp import fields,models,api


class account_journal(models.Model):
    _inherit = 'account.journal'

    reopen_posted=fields.Boolean('Allow Update of Posted Entries', help="Allows to reopen posted invoices, sets the move state to unposted")
