from openerp import fields,models,api,_
import time


class purchase_order(models.Model):
    _inherit = 'purchase.order'


    @api.multi
    def action_reopen(self):
        """ Changes SO from to draft.
        @return: True
        """
        
        attachment_obj = self.env['ir.attachment']


        now = ' ' + _('Invalid') + time.strftime(' [%Y%m%d %H%M%S]')
        for order in self:
            order.picking_ids and order.picking_ids.action_reopen()
            order.invoice_ids and order.invoice_ids.action_reopen()            
            
            order.signal_workflow('purchase_cancel')
            order.write({'state':'draft','shipped':False})
            order.order_line.write({'state':'draft'})
            order.delete_workflow()
            order.create_workflow()
            #order.invoice_ids and order.invoice_ids.action_cancel_draft()
                     
            attachments = attachment_obj.search([('res_model','=','purchase.order'),('res_id','=',order.id)])
            for attachment in attachments:
                vals = {
                    'name': attachment.name.replace('.pdf', now+'.pdf'),
                    'datas_fname': attachment.datas_fname.replace('.pdf.pdf', now+'.pdf.pdf')
                       }
                attachment.write(vals)
            
        return True


